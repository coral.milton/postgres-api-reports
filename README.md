# Data WareHouse Proyect 

# General Objective 

Design and develop integrated components in web applications using current standards. Conduct the analysis, design, and implementation of a data warehouse that serves as a foundation for generating reports to assist in decision-making.

# Instructions for the Proyect

The project involves creating a modular web application that allows access to information from a multidimensional database. Architecturally, the application is divided into three parts: Front-end, Back-end, and Data Persistence.

For the front-end, a user interface should be developed using HTML, CSS, and JavaScript. It is recommended to use libraries such as Bootstrap, JQuery, Datatables, and Highcharts.

In the second part, the back-end will be developed using Node.js. Communication between the front-end and the back-end will be handled through RESTful services.

Finally, data persistence will be managed by a multidimensional database with PostgreSQL.

From a functional perspective, the application should include two modules: a) Reports and b) Maintenance.

The Maintenance module will allow for the management (adding, modifying, deleting) of existing information in the database. The Reports module will display the stored information through a dashboard, featuring at least two (2) different statistical charts and three (3) tables. Data can be analyzed using at least two (2) different filters. The table data should be exportable to spreadsheets and PDF files. Below is a component diagram of the application.